import unittest   # The test framework
from Tools import utilities, sqlite_table, decorators
from pathlib import Path 
import sqlite3
import app

@decorators.parameters_accepted(int, (int,float))
def test(arg1, arg2):
  return arg1 * arg2

class Test_Utilities(unittest.TestCase):
    def test_is_an_allowed_file(self):  
        """Test utilities.is_an_allowed_file

        utilities.is_an_allowed_file() return a boolean (True or False) according to the
        extension of the object. True if it's allowed, False otherwise"""

        self.assertEqual(utilities.is_an_allowed_file("test.jpg"), True)

        self.assertEqual(utilities.is_an_allowed_file("test.gif"), False)

        self.assertEqual(utilities.is_an_allowed_file(" "), False)

        self.assertEqual(utilities.is_an_allowed_file(1), False)

    def test_getImageInfos(self):
        """Test utilities.getImageInfos

        utilities.getImageInfos() should raise a FileNotFoundError or TypeError if the filename is wrong
        and a tuple object if the filename is well writed"""

        with self.assertRaises(FileNotFoundError):
          utilities.getImageInfos("test")

        with self.assertRaises(TypeError):
          utilities.getImageInfos(1)

        self.assertEqual(type(utilities.getImageInfos("Tests\\image.jpg")), tuple)

    def test_create_thumbnail(self):
        """Test utilities.create_thumbnail

        utilities.create_thumbnail() should return a tuple as response (Path, True) if everything is correct
        and (str, False) if not, especially if the id value is not available"""

        self.assertEqual(utilities.create_thumbnail(5, "Tests\\image.jpg")[1], False)

    def test_clean_directory(self):
      self.assertRaises(FileNotFoundError, utilities.clean_directory, Path('./directory')) 

class Test_Sqlite_tables(unittest.TestCase):
    def test_create_image_infos_table(self):
      # Test de creation de la table image
      self.assertEqual(sqlite_table.create_image_infos_table()["Message"], "Table created successfully ✔️")  
      sqlite_table.delete_all()

    def test_add_image_informations(self):
      # Test d'ajout d'informations dans la table image
      # Le resultat attendu est un dictionnaire
      sqlite_table.create_image_infos_table()
      self.assertEqual(type(sqlite_table.add_image_informations("tests")), dict)  
      sqlite_table.delete_all()

    def test_read_image_informations(self):
      # Test de lecture d'un element contenu dans la table
      # Retourne False si aucune information n'est retournée
      sqlite_table.create_image_infos_table()
      self.assertFalse(sqlite_table.read_image_informations(1)[1])  
      sqlite_table.delete_all()

    def test_delete_all(self):
      sqlite_table.delete_all()

      with self.assertRaises(sqlite3.OperationalError):
          sqlite_table.read_image_informations(1)

    def test_read_all_images(self):
      sqlite_table.create_image_infos_table()
      self.assertEqual(type(sqlite_table.read_all_images()), dict)  
      sqlite_table.delete_all() 

class Test_Decorators(unittest.TestCase):
    def test_test_parameters(self):
      with self.assertRaises(AssertionError):
        test('7', 8)

class Test_Flask_app(unittest.TestCase):
    def setUp(self):
        app.app.testing = True
        self.app = app.app.test_client()        

    def test_home(self):
      result = self.app.get('/')
      self.assertEqual(result.get_json()["Message"], "💻 Root page")

    def test_uploadNewImage(self):
      self.assertEqual(2, 1+1)  

    def test_getImageInformations(self):
      sqlite_table.delete_all()
      sqlite_table.create_image_infos_table()
      result = self.app.get('/images/1')
      self.assertEqual(result.get_json()["Message"], "Sorry, This image id is not available") 

    def test_getGeneratedImage(self):
      result = self.app.get('/thumbnails/1')
      self.assertEqual(result.get_json()["Message"], "Sorry, this image id is not available")  

      result = self.app.get('/thumbnails/e')
      self.assertEqual(result.get_json()["Message"], "Value error, id must be an integer")  
      
if __name__ == '__main__':
    unittest.main()