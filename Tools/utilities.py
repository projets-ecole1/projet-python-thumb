#importing os module
import os, shutil
from Tools import sqlite_table
from pathlib import Path 
from flask import escape
# Importing Pillow Python Imaging Library
from PIL import Image
from PIL.ExifTags import TAGS

# Folder used to save uploaded image during the first processing
UPLOAD_FOLDER = Path('..\\projet-python-thumb\\image')

# Allowed extensions for the thumbnail generator (Trying to avoid XSS problems)
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

# Permet de tester la validité d'un nom de fichier par rapport aux extensions
def is_an_allowed_file(filename):
    """Method used to test the filename validity considering allowed extensions

    Args:
        filename (str): the filename

    Returns:
        [boolean]: Return True if the filename extension is present in allowed extensions (png, jpg, jpeg)
    """
    filename = escape(filename)

    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Permet de retourner les informations (metadatas) d'une image
def getImageInfos(filename):   
    """Method used to get images informations

    Args:
        filename (str): the filename

    Returns:
        [tuple]: Return the tuple containing images informations
    """
    path = Path(filename)

    # Instanciation d'un objet image
    image = Image.open(filename)

    # Calcul de la taille de l'image en kilo octets
    weight = (path.stat().st_size)/1024

    # Construction de la tuple formée par les informations de l'image
    infos = (filename.rsplit('\\', 1)[1], image.size[0], image.size[1], weight, image.format)
    
    return infos

# Permet de creer les thumbnail d'une image
def create_thumbnail(id, path):
    """Method used to create a thumbnail of an image 

    Args:
        id : image's id
        path : Path to the original image

    Returns:
        [Path or str]: Return a path to the thumbnail created or an error string
    """
    # Taille du thumbnail voulu
    size = (128, 128)

    # Recuperation et changement de l'extension du fichier initial
    result = os.path.splitext(path)[0] + ".thumbnail"

    # denomination finale du fichier à generer
    filename = f"./image/thumbnails/{id}.jpg"

    query_table = sqlite_table.read_image_informations(id)

    # Si l'id demandé est présente en base de donnée
    if query_table[1] == True:

        # Condition a remplir pour debuter la conversion, verifier que le fichier n'ai pas l'extension .thumbnail
        if str(path) != result:
            try:
                with Image.open(path).convert('RGB') as im:
                    im.thumbnail(size)
                    im.save(filename, "JPEG")
                    return  Path(f'{UPLOAD_FOLDER}\\{filename}'), True

            except OSError as e:
                return str(e), False
    else:
        return "This image id is not already added in the table", False

# Permet de supprimer le contenu des repetoirs passé en parametre
def clean_directory(path):
    """Method used to test the filename validity considering allowed extensions

    Args:
        path (Path): the path of the directory wich need to be cleaned

    Returns:
        Error message if the path is wrong
    """
    folder = path
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)

        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)

        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
