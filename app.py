# -*- coding: utf-8 -*-
import io, os, secrets 
from pathlib import Path 
from flask import Flask, escape, jsonify, request, redirect, url_for, flash, send_from_directory
from Tools import sqlite_table, utilities
from werkzeug.utils import secure_filename

app = Flask(__name__)

app.config["SECRET_KEY"] = secrets.token_urlsafe(16)

app.config['UPLOAD_FOLDER'] = utilities.UPLOAD_FOLDER

sqlite_table.delete_all()

sqlite_table.create_image_infos_table()

@app.route('/', methods=['GET'])
def home():
    body = {}
    body["Message"] = "💻 Root page"
    return jsonify(body)

@app.route('/images', methods=['POST'])
def uploadNewImage():
    body = {}

    # check if the post request has the file part
    if 'file' not in request.files:
        body["Message"] = "No file part"
        return jsonify(body)

    file = request.files['file']
    # if user does not select file, browser also
    # submit an empty part without filename
    
    if file.filename == '':
        body["Message"] = "No selected file"
        return jsonify(body)

    if file and utilities.is_an_allowed_file(file.filename):
        filename = secure_filename(file.filename)

        file.save(os.path.join(f'{utilities.UPLOAD_FOLDER}\\loaded', filename))

        infos = utilities.getImageInfos(f'{utilities.UPLOAD_FOLDER}\\loaded\\{filename}')

        sqlite_table.create_image_infos_table()

        resultat = sqlite_table.add_image_informations(infos)

        body["Message"] = f"File received ✔️ : {file.filename}"
        
        body["image id"] = resultat["image id"]

        return jsonify(body)
    else:
        body["Message"] = "Sorry, This kind of file is not allowed ⚠️"
        return jsonify(body)

@app.route('/images/<id>', methods=['GET'])
def getImageInformations(id=0):
    """Methode qui retourne les metadatas d'une image dont l'id es passé en parametres

    Args:
        id (int): id de l'image 

    Returns:
        [type]: Un objet JSON 
    """
    body = {}
    body["Status"] = "Failed ❌"
    
    try:
        # L'id passé en parametres
        content = escape(id)

        # Conversion de l'id passé en parametre en entier
        idValue = int(content)

        # On recupere les informations associé à cet id en base de données
        image_infos = sqlite_table.read_image_informations(idValue)
        
        if "image" in image_infos[0]:
            # Recuperation du nom de l'image depuis les résultas issus de la database
            image_Path = Path(f'{utilities.UPLOAD_FOLDER}\\loaded\\{image_infos[0]["image"][1]}')

            # Conversion de l'image en thumbnail au format JPG
            convert_result = utilities.create_thumbnail(idValue, image_Path)

            # Si le conversion se deroule bien, on releve les informations à afficher
            if convert_result[1] == True:
                id = os.path.splitext(convert_result[0])[0].rsplit('\\', 1)[1]
                before = {
                    "Name : ": image_infos[0]["image"][1],
                    "Size : ": f'{image_infos[0]["image"][2]}px X {image_infos[0]["image"][3]}px',
                    "Weight : ": f'{image_infos[0]["image"][4]} Ko',
                    "Format : ": image_infos[0]["image"][5]
                }
                after = {
                    "🔗 Link to the generated thumbnail : ": url_for('getGeneratedImage', id = id, _external=True),
                    "Name : ": id + ".jpg",
                    "Format : ": "JPG"
                }
                body["Status"] = "Success ✔️"
                body["Message"] = { "Before conversion : ":before, "After conversion : ":after }

            else:
                body["Message"] = f"Conversion to thumbnail has failed, {convert_result[0]}"
    
    except ValueError as v:
        body["Message"] = f"The id must be an integer, please try again : Cause - {v}"

    except TypeError:
        body["Message"] = "Sorry, This image id is not available"
    
    return jsonify(body)

@app.route('/thumbnails/<id>', methods=['GET'])
def getGeneratedImage(id=0):  
    """Methode qui permet d'afficher l'image reduite issu de la conveersion en thumbnail

    Args:
        id (int): l'id de l'image

    Raises:
        IndexError: Retourne un message d'erreur au cas où l'id n'existe pas

    Returns:
        [image] ou [message]: la miniature générée ou un message d'erreur
    """
    try:
        idValue = int(escape(id))
        
        id_exist = sqlite_table.read_image_informations(idValue)[1]

        if id_exist:
            filename = f"{idValue}.jpg"
            return send_from_directory(utilities.UPLOAD_FOLDER / 'thumbnails', filename)
        else:
            raise IndexError

    except ValueError:
        return {"Message": "Value error, id must be an integer"}

    except IndexError:
        return {"Message": "Sorry, this image id is not available"}
        
if __name__ == '__main__':
    app.run()